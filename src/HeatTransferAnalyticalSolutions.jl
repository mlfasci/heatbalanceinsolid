module HeatTransferAnalyticalSolutions

include("./expint.jl")
export ILS

"""
Infinite Line source solution
"""
ILS(Fo::AbstractFloat) = 1/2*expint(1/(4*Fo)) #non-dimensional expression depending on Fo (Fourier)
ILS(t::T,r::T,α::T) where {T<:AbstractFloat} = ILS( (α*t)/r^2 ) #dimensional expression of the temperature as a function of time t, thermal diffusivity α and radius r

end